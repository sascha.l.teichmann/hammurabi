# Hammurabi

A [Go](https://golang.org) clone of the classic text-based strategy game [Hamurabi](https://en.wikipedia.org/wiki/Hamurabi_%28video_game%29).  
Based on this Python [implementation](https://github.com/wzhishen/hammurabi).

This is Free Software under the terms of the [Apache License Version 2.0](LICENSE).
