// This is Free Software licensed under the Apache License, Version 2.0 (the "License").
//
// Copyright 2021 by the contributors.
// See CONTRIBUTORS for details.
//
// SPDX-License-Identifier: Apache-2.0
// License-Filename: LICENSE
//
// Author(s):
//  * Sascha L. Teichmann (sascha.teichmann@intevation.de)

package main

import (
	"fmt"
	"math/rand"
)

type state struct {
	starved          int
	immigrants       int
	population       int
	harvested        int
	bushelsPerAcre   int
	ratsAte          int
	bushelsInStorage int
	acresOwned       int
	costPerAcre      int
	plagueDeath      int
	year             int
}

type stateFn func(*state) stateFn

func main() {

	s := state{
		starved:          0,
		immigrants:       5,
		population:       100,
		harvested:        3000,
		bushelsPerAcre:   3,
		ratsAte:          200,
		bushelsInStorage: 2800,
		acresOwned:       1000,
		costPerAcre:      19,
		plagueDeath:      0,
		year:             1,
	}

	for next := (*state).introduction; next != nil; next = next(&s) {
	}
}

func (s *state) printStats() {
	fmt.Printf(`O great Hammurabi!
You are in year %d of your ten year rule.
In the previous year %d people starved to death.
In the previous year %d people entered the kingdom.
The population is now %d.
We harvested %d bushels at %d bushels per acre.
Rats destroyed %d bushels, leaving %d bushels in storage.
The city owns %d acres of land.
Land is currently worth %d bushels per acre.
There were %d death from the plague.
`,
		s.year,
		s.starved, s.immigrants,
		s.population,
		s.harvested, s.bushelsPerAcre,
		s.ratsAte, s.bushelsInStorage,
		s.acresOwned,
		s.costPerAcre,
		s.plagueDeath)
}

func (s *state) introduction() stateFn {
	fmt.Println(`Congratulations, you are the newest ruler of ancient Samaria, elected
for a ten year term of office. Your duties are to dispense food, direct
farming, and buy and sell land as needed to support your people. Watch
out for rat infestations and the plague! Grain is the general currency,
measured in bushels. The following will help you in your decisions:

  * Each person needs at least 20 bushels of grain per year to survive.

  * Each person can farm at most 10 acres of land.

  * It takes 2 bushels of grain to farm an acre of land.

  * The market price for land fluctuates yearly.

Rule wisely and you will be showered with appreciation at the end of
your term. Rule poorly and you will be kicked out of office!`)
	return (*state).reign
}

func getInt(message string) int {
	fmt.Print(message)
	var value int
	fmt.Scanf("%d", &value)
	if value < 0 {
		return -value
	}
	return value
}

func (s *state) askBuyLand() int {
	acres := getInt("How may acres of land will you buy? (0 to DECLINE) ")
	for acres*s.costPerAcre > s.bushelsInStorage {
		fmt.Printf("O great Hammurabi, we have only %d bushels of grain!\n",
			s.bushelsInStorage)
		fmt.Printf("%d acres are too much to afford to buy!\n", acres)
		acres = getInt(
			fmt.Sprintf("Again, how many land will you buy? (0 to DECLINE)\n"+
				"[HINT: The maximum of current purchasing power is %d acres.] ",
				s.bushelsInStorage/s.costPerAcre))
	}
	return acres
}

func (s *state) askSellLand() int {
	acres := getInt("How may acres of land will you sell? ")
	for acres > s.acresOwned {
		fmt.Printf("O great Hammurabi, we have only %d acres of land!\n",
			s.acresOwned)
		fmt.Printf("%d exceed our land limit!\n", acres)
		acres = getInt(fmt.Sprintf("Again, how much land will you sell?\n"+
			"[HINT: Current land price is %d.] ", s.costPerAcre))
	}
	return acres
}

func (s *state) buyLand() {
	bought := s.askBuyLand()
	if bought > 0 {
		s.acresOwned += bought
		s.bushelsInStorage -= bought * s.costPerAcre
	} else {
		sold := s.askSellLand()
		s.acresOwned -= sold
		s.bushelsInStorage += sold * s.costPerAcre
	}
}

func (s *state) askFeedPeople() int {
	bushels := getInt("How many bushels of grain will you feed to the people? ")
	for bushels > s.bushelsInStorage {
		fmt.Printf("O great Hammurabi, we have only %d bushels of grain!\n",
			s.bushelsInStorage)
		fmt.Printf("%d bushels exceed our grain reserves!\n", bushels)
		bushels = getInt(fmt.Sprintf(
			"Again, how much grain will you feed to the people?\n"+
				"[HINT: In order for everyone to survive, the current population "+
				"needs at least a total amount of %d bushels.] ", s.population*20))
	}
	return bushels
}

func (s *state) feedPeople() int {
	fed := s.askFeedPeople()
	s.bushelsInStorage -= fed
	return fed
}

func (s *state) doPlague() {
	if rand.Intn(100) >= 15 {
		fmt.Println("***Thank God! No plague this year***")
		return
	}
	fmt.Printf(
		"***O great Hammurabi, unluckily a horrible PLAGUE happened! "+
			" %d people died***\n", s.population/2)
	s.plagueDeath = s.population / 2
}

func (s *state) doStarvation(fed int) bool {
	starved := s.population - fed/20
	if starved == 0 {
		fmt.Println(
			"***O brilliant Hammurabi! This year we have NO people starved***")
		return false
	}
	fmt.Printf(
		"***O great Hammurabi, unfortunately this year we have %d PEOPLE STARVED***\n",
		starved)
	if float64(starved) > 0.45*float64(s.population) {
		return true
	}
	s.population -= starved
	s.starved = starved
	return false
}

func (s *state) doImmigration() {
	if s.starved > 0 {
		fmt.Println(
			"***O great Hammurabi, due to starvation, " +
				"NO IMMIGRANTS this year come to our kingdom***")
		return
	}
	s.immigrants = (20*s.acresOwned+s.bushelsInStorage)/(100*s.population) + 1
	s.population += s.immigrants
	fmt.Printf(
		"***O great Hammurabi, this year %d immigrants come to our kingdom***\n",
		s.immigrants)
}

func (s *state) askPlantLand() int {
	acres := getInt("How many acres of land will you plant with seed? ")
	for {
		if acres > s.population*10 {
			fmt.Printf(
				"O great Hammurabi, we have only %d people that can farm the land!\n",
				s.population)
			fmt.Printf(
				"%d acres are too many for us! We need more people to do the job!\n",
				acres)
			acres = getInt(fmt.Sprintf(
				"Please factor our manpower. Again, how much land will you plant?\n"+
					"[HINT: The maximum possible acres of current"+
					" total labor force that can afford to farm is %d acres.] ",
				s.population*10))
			continue
		}

		if s.bushelsInStorage < acres*2 {
			fmt.Printf(
				"O great Hammurabi, we have only %d bushels to spend farming the land!\n",
				s.bushelsInStorage)
			fmt.Printf(
				"%d acres are too many for us! We lack enough bushels to start our farming!\n",
				acres)
			acres = getInt(
				fmt.Sprintf(
					"Please factor our funds. Again, how much land will you plant with seed?\n"+
						"[HINT: The maximum possible acres of we can currently farm are %d.] ",
					s.bushelsInStorage/2))
			continue
		}

		break
	}
	return acres
}

func (s *state) plantLand() int {
	planted := s.askPlantLand()
	s.bushelsInStorage -= 2 * planted
	return planted
}

func (s *state) reign() stateFn {

	s.printStats()
	s.buyLand()
	fed := s.feedPeople()
	planted := s.plantLand()
	s.doPlague()
	if s.doStarvation(fed) {
		return s.dismiss()
	}
	s.doImmigration()
	s.doHarvest(planted)
	s.doRatsInfestation()
	s.adjustLandPrice()

	if s.year++; s.year > 10 {
		return (*state).retire
	}

	return (*state).reign
}

func (s *state) adjustLandPrice() {
	s.costPerAcre = rand.Intn(7) + 17
	fmt.Printf(
		"***O great Hammurabi, the price of land will be "+
			"%d bushels per acre next year***", s.costPerAcre)
}

func (s *state) doRatsInfestation() {
	if rand.Intn(100) >= 40 {
		fmt.Println("***Thank God! No rat infestation occurred this year***")
		return
	}
	s.ratsAte = (rand.Intn(21) + 10) * s.harvested / 100
	fmt.Printf(
		"***O great Hammurabi, unluckily a horrible RAT INFESTATION happened! "+
			"%d bushels of grain were destroyed***\n", s.ratsAte)
	s.bushelsInStorage -= s.ratsAte
}

func (s *state) doHarvest(planted int) {
	s.bushelsPerAcre = rand.Intn(8) + 1
	s.harvested = planted * s.bushelsPerAcre
	s.bushelsInStorage += s.harvested
	fmt.Printf(
		"***O great Hammurabi, this year we harvest %d bushels of grain***\n",
		s.harvested)
}

func (s *state) dismiss() stateFn {
	fmt.Printf(
		`***O great Hammurabi, we really regret to inform you that this
year %d people have been starved.
This represents more than 45%% of total population in your Samaria kingdom.
According to our laws, you're now unfortunately given an immediate
dismissal request***
`, s.starved)
	return (*state).dismissed
}

func (s *state) dismissed() stateFn {
	fmt.Printf(
		`========================== G A M E ===== O V E R ==========================
       O great Hammurabi, we're truly regretful that in year %d of
       your ten year rule, you lost your imperial authority.

                 *** THANK YOU FOR PLAYING HAMMURABI! ***
`, s.year)
	return nil
}

func (s *state) retire() stateFn {
	fmt.Printf(
		`===================== C O N G R A T U L A T I O N S !======================
    Hammurabi your highness, you've successfully gone through your ten
    year term of office.
    In the end of your term of reign, the Samaria Kingdom:

  * owns %d acres of wealthy land;
  * owns %d bushels of grain in storage;
  * has a total population of %d;
  * has %d people starved.

%s
%s
%s
%s
=================== G A M E ===== A C C O M P L I S H E D =================

                  *** THANK YOU FOR PLAYING HAMMURABI! ***
`,
		s.acresOwned,
		s.bushelsInStorage,
		s.population,
		s.starved,
		evalAcres(s.acresOwned),
		evalBushels(s.bushelsInStorage),
		evalPopulation(s.population),
		evalStarved(s.starved))
	return nil
}

func evalAcres(acres int) string {
	switch {
	case acres > 1550:
		return "O great Hammurabi, you've done very well in keeping " +
			"the realm of our kingdom increasing. Fantastic!"
	case acres > 1000:
		return "O great Hammurabi, you've well maintained our land " +
			"size as it was ten years ago, though our domain expanded little!"
	case acres > 500:
		return "O great Hammurabi, you've lost some of our land! " +
			"That's a pity. You could have done better!"
	default:
		return "O great Hammurabi, our fruitful land has been shrinked " +
			"dramatically under your rule. That's really not good!"
	}
}

func evalBushels(bushels int) string {
	switch {
	case bushels > 18000:
		return "Nice job! You have earned a lot to feed and finance our homeland. " +
			"That's really cool!"
	case bushels > 10000:
		return "Steady increase of bushels. Our country is wealthy!"
	case bushels > 2800:
		return "Fine policies, but haven't earned much. " +
			"We need more bushels to thrive!"
	default:
		return "Oops, you haven't handled your financial task well. " +
			"So much grain lost!"
	}
}

func evalPopulation(people int) string {
	switch {
	case people > 200:
		return "There are more than twice people now living in Samaria as ten years ago."
	case people > 130:
		return "During your term of office, population here increases pretty steadily."
	case people > 95:
		return "During the ten years, total number of people in Samaria changed little."
	default:
		return "In the end of your reign, total population decreased."
	}
}

func evalStarved(starved int) string {
	switch {
	case starved > 70:
		return "In the end of your term more than 70 citizens starved to death. " +
			"That's an extremely serious problem!"
	case starved > 30:
		return "In the end of your term at least 30 people died from starvation. " +
			"Every citizen's basic living requirements should be met!"
	case starved > 0:
		return "In the end of your term, still some people starved to die. " +
			"This could have been avoided by a reasonable farming policy."
	default:
		return "That's great because now no one starved in our country!"
	}
}
